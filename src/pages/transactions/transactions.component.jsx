import React,{useState,useEffect} from 'react';
import { MDBDataTableV5 } from 'mdbreact';
import { getTransaction } from "../../actions/getTransactions";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

export default function Transactions() {
  const [datatable, setDatatable] = useState([]);
  const [listTransacts, setListTransacts] = useState([]);

  useEffect(
    () => {
      getTransaction(sessionStorage.idU).then( (data)=> {
        setListTransacts(data);
      })
      .catch((error) => console.log(error))
    },
    [],
  );

  return (
  <>
  <h1>Transactions</h1>
    <BootstrapTable
        data={listTransacts}
        striped
        hover
        condensed
        search
        exportCSV
        options
      >
        <TableHeaderColumn dataField="id" isKey dataSort>
          id
        </TableHeaderColumn>
        <TableHeaderColumn dataField="prix" editable={false} dataSort>
          Prix
        </TableHeaderColumn>
        <TableHeaderColumn dataField="numclient" editable={false} dataSort>
          Identifiant du client
        </TableHeaderColumn>
        <TableHeaderColumn dataField="marchandId" editable={false} dataSort>
          Marchand
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="updatedAt"
          editable={false}
          dataFormat={format}
        >
          Date de commande
        </TableHeaderColumn>
      </BootstrapTable>
      </>
  );
};

function format(cell, row) {
  return new Date(cell).toLocaleString();
}