import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "react-bootstrap-table/css/react-bootstrap-table.css";

import { getShopByUser } from "../../actions/getShopByUser";
import { ResponsiveBar } from '@nivo/bar'


export default function Home () {
  const [listeShop, setListeShop] = useState([]);
  const [shopSelectedId, setShopSelected] = useState(null);
  const [goViewShop, setGoViewShop] = useState(false);
  const options = {
    onRowClick: function (row) {
      setShopSelected(row.id);
      setGoViewShop(true);
    },
  };

  useEffect(() => {
    getShopByUser(sessionStorage.idU).then((data) => {
      console.log(data);
      setListeShop(data);
    });
  }, []);

  

  return (
    <>
      <h1>Shop</h1>

      {goViewShop && <Redirect to={`/view-shop/` + shopSelectedId} />}
      <BootstrapTable
        data={listeShop}
        striped
        hover
        condensed
        search
        exportCSV
        options={options}
      >
        <TableHeaderColumn dataField="id" isKey dataSort>
          id
        </TableHeaderColumn>
        <TableHeaderColumn dataField="nomSociete" editable={false} dataSort>
          Nom
        </TableHeaderColumn>
        <TableHeaderColumn dataField="contact" editable={false} dataSort>
          Contact
        </TableHeaderColumn>
        <TableHeaderColumn dataField="devise" editable={false} dataSort>
          Devise
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="updatedAt"
          editable={false}
          dataFormat={format}
        >
          Membre depuis
        </TableHeaderColumn>
      </BootstrapTable>
    </>
  );
};

function format(cell, row) {
  return new Date(cell).toLocaleString();
}

class ActiveFormatter extends React.Component {
  render() {
    return <input type="checkbox" checked={this.props.active} />;
  }
}

function activeFormatter(cell, row) {
  return <ActiveFormatter active={cell} />;
}
