import React, { useState, useEffect } from "react";
import { Redirect, useParams } from "react-router-dom";
import { Button, Form, Col, Row, Container } from "react-bootstrap";

import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";

import { getUserById } from "../../actions/getUserById";
import { modifUser } from "../../actions/modifUser";
import { deleteUser } from "../../actions/deleteUser";
import AlertBar from "../../components/alert-bar/alert-bar.component";

import './detail-user.styles.scss';

const DetailUser = () => {
    
  let { id } = useParams();
  const [userSelected, setUserSelected] = useState(false);
  const [goViewUser, setGoViewUser] = useState(false);
  const [show, setShow] = useState(false);
  const [status, setStatus] = React.useState();
  const [isAdmin, setAdmin] = React.useState();
  const handleChange = (event) => {
    setStatus(event.target.value);
  };

  const handleChangeAdmin = (event) => {
    setAdmin(event.target.value);
  };
  
  function handleSubmit(event) {
    event.preventDefault();
    const data = Object.fromEntries(new FormData(event.target).entries());
    modifUser(id, data).then(function (response) {
      setShow(true);
    });
  }

  const deleteMyUser = () => {
    // event.preventDefault();
    // const data = Object.fromEntries(new FormData(event.target).entries());
    deleteUser(id).then(function (response) {
      setGoViewUser(true);
    });
  };

  const hidePop = () => {
    setShow(false);
  }

  useEffect(() => {
    getUserById(id).then((data) => {
      // console.log(data);
      setStatus(data.actif);
      setAdmin(data.isAdmin);
      setUserSelected(data);

      // console.log(userSelected);
      //setListeUser(data);
    });
  }, []);

  return (
    <div className="detail-user-container">
      {show && (
        <AlertBar hidePop={hidePop} variant="success">
          Modification realisé
        </AlertBar>
      )}
      {goViewUser && <Redirect to={`/home-admin/`} />}
      {userSelected !== false && (
        <>
          <h1>User : {userSelected.username}</h1>
          <Form onSubmit={handleSubmit}>
            <Form.Row>
              <Col>
                <Form.Group>
                  <Form.Label>Nom :</Form.Label>
                  <Input
                    type="text"
                    name="lastname"
                    defaultValue={userSelected.firstname}
                    placeholder="Nom"
                  />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group>
                  <Form.Label>Prenom :</Form.Label>
                  <Input
                    type="text"
                    name="firstname"
                    defaultValue={userSelected.lastname}
                    placeholder="Prenom"
                  />
                </Form.Group>
              </Col>
            </Form.Row>
            <Container>
              <Row>
                <Col md={4}>
                  <Form.Group>
                    <InputLabel id="demo-customized-select-label">
                      Status
                    </InputLabel>
                    <Select
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      name="actif"
                      value={status}
                      onChange={handleChange}
                    >
                      <MenuItem value={true}>actif</MenuItem>
                      <MenuItem value={false}>inactif</MenuItem>
                    </Select>
                  </Form.Group>
                </Col>

                <Col md={{ span: 4, offset: 4 }}>
                  <Form.Group>
                    <InputLabel id="demo-customized-select-label">
                      Droit admin
                    </InputLabel>
                    <Select
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      name="isAdmin"
                      value={isAdmin}
                      onChange={handleChangeAdmin}
                    >
                      <MenuItem value={true}>admin</MenuItem>
                      <MenuItem value={false}>non admin</MenuItem>
                    </Select>
                  </Form.Group>
                </Col>
              </Row>
            </Container>

            <Button block type="submit">
              Modifier
            </Button>
            <Button
              block
              variant="danger"
              onClick={() => {
                deleteMyUser();
              }}
            >
              Suprimer
            </Button>
          </Form>
        </>
      )}
    </div>
  );
}

export default DetailUser;


// const BootstrapInput = withStyles((theme) => ({
//   root: {
//     'label + &': {
//       marginTop: theme.spacing(3),
//     },
//   },
//   input: {
//     borderRadius: 4,
//     position: 'relative',
//     backgroundColor: theme.palette.background.paper,
//     border: '1px solid #ced4da',
//     fontSize: 16,
//     padding: '10px 26px 10px 12px',
//     transition: theme.transitions.create(['border-color', 'box-shadow']),
//     // Use the system font instead of the default Roboto font.
//     fontFamily: [
//       '-apple-system',
//       'BlinkMacSystemFont',
//       '"Segoe UI"',
//       'Roboto',
//       '"Helvetica Neue"',
//       'Arial',
//       'sans-serif',
//       '"Apple Color Emoji"',
//       '"Segoe UI Emoji"',
//       '"Segoe UI Symbol"',
//     ].join(','),
//     '&:focus': {
//       borderRadius: 4,
//       borderColor: '#80bdff',
//       boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
//     },
//   },
// }))(InputBase);
