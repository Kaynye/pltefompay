import React, { useState } from "react";
import { Button, Form, FormControl, FormLabel, Col } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import "./register.styles.scss";
import { register } from "../../actions/register";
import useAuth from "../../hooks/useAuth";
import AlertBar from "../../components/alert-bar/alert-bar.component";

export default function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [goLogin, goToLogin] = useState(false);
  const { selectors, actions } = useAuth();
  const [showCreate, setShowCreate] = useState(false);
  const [showError, setShowError] = useState(false);
  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    const data = Object.fromEntries(new FormData(event.target).entries());

    register(data).then(function (response) {
      // actions({ ...data });
      // goToLogin(true);
      console.log('yessssss')
      setShowCreate(true);
    });
  }
  function goLoginE() {
    goToLogin(true);
  }

  const hidePopCreate = () => {
    setShowCreate(false);
  }


  return (
    <div className="register">
      {showCreate && (
        <AlertBar variant="success" hidePop={hidePopCreate}>
          Compte crée en attente de validation par un administrateur
        </AlertBar>
      )}
      {goLogin && <Redirect to="/login" />}
      {selectors.isLogged() && <Redirect to="/home" />}
      <Form onSubmit={handleSubmit}>
        <Form.Row>
          <Col>
            <Form.Group>
              <Form.Label>Nom</Form.Label>
              <Form.Control type="text" name="lastname" placeholder="Nom" />
            </Form.Group>
          </Col>
          <Col>
            <Form.Group>
              <Form.Label>Prenom</Form.Label>
              <Form.Control type="text" name="firstname" placeholder="Prenom" />
            </Form.Group>
          </Col>
        </Form.Row>

        <Form.Group controlId="exampleForm.ControlInput1">
          <Form.Label>Adresse email</Form.Label>
          <Form.Control
            type="email"
            name="username"
            onChange={(e) => setEmail(e.target.value)}
            placeholder="name@example.com"
          />
        </Form.Group>

        <Form.Group controlId="exampleForm.ControlInput1">
          <FormLabel>Password</FormLabel>
          <FormControl
            // value={password}
            name="password"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        <Form.Label onClick={goLoginE}>deja membre ? se connecter. </Form.Label>
        <Button block bsSize="large" disabled={!validateForm()} type="submit">
          Creer compte
        </Button>
      </Form>
    </div>
  );
}
