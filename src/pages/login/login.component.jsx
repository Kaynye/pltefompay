import React, { useState } from "react";
import {
  Form,
  Button,
  FormGroup,
  FormControl,
  FormLabel,
} from "react-bootstrap";
import "./login.styles.scss";
import useAuth from "../../hooks/useAuth";
import { Redirect } from "react-router-dom";
import AlertBar from "../../components/alert-bar/alert-bar.component";
// import Select from '@material-ui/core/Select';

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [goRegister, goToRegister] = useState(false);
  const { selectors, actions } = useAuth();

  const [show, setShow] = useState(false);

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    const data = Object.fromEntries(new FormData(event.target).entries());
    actions({ ...data }).then((data) => {
      setShow(true);
    });
  }

  const hidePop = () => {
    setShow(false);
  }

  function go() {
    // console.log('lgo');
    goToRegister(true);
  }

  return (
    <div className="login">
      {/* <span>{selectors.isLogged() ? "Connected" : "Not connected"}</span> */}
      {show && (
        <AlertBar variant="error" hidePop={hidePop}>
          Email ou mot de passe incorect
        </AlertBar>
      )}
      {goRegister && <Redirect to="/register" />}
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email">
          <FormLabel>Email</FormLabel>
          <FormControl
            autoFocus
            type="email"
            name="username"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <FormLabel>Password</FormLabel>
          <FormControl
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            name="password"
            type="password"
          />
        </FormGroup>
        <Form.Label onClick={go}>S'inscrire </Form.Label>
        <Button block bsSize="large" disabled={!validateForm()} type="submit">
          Login
        </Button>
        {selectors.isLogged() &&
          (selectors.isAdmin() ? (
            <Redirect to="/home-admin" />
          ) : (
            <Redirect to="/home" />
          ))}
      </form>
    </div>
  );
}

export default Login;
