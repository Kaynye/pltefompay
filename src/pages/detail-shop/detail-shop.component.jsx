import React, { useState, useEffect } from "react";
import { Redirect, useParams } from "react-router-dom";
import { Button, Form, Col, Row, Container, Image } from "react-bootstrap";
// import "./styles/Login.css";
import { getShopById } from "../../actions/getShopById";
import { modifShop } from "../../actions/modifShop";
import { deleteUser } from "../../actions/deleteUser";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import AlertBar from "../../components/alert-bar/alert-bar.component";
import { getExchangeRate } from "../../actions/ExchangeRate";

const DetailShop = () => {
  let { id } = useParams();
  const [shopSelected, setshopSelected] = useState(false);
  const [goViewShop, setgoViewShop] = useState(false);
  const [show, setShow] = useState(false);
  const [devise, setDevise] = React.useState();
  const [isAdmin, setAdmin] = React.useState();
  const [exchangeRate, setExchangeRate] = useState([]);

  const handleChange = (event) => {
    setDevise(event.target.value);
  };

  const handleChangeAdmin = (event) => {
    setAdmin(event.target.value);
  };

  function handleSubmit(event) {
    event.preventDefault();
    const data = Object.fromEntries(new FormData(event.target).entries());
    modifShop(id, data).then(function (response) {
      setShow(true);
    });
  }

  const hidePop = () => {
    setShow(false);
  }
  const deleteMyUser = () => {
    // event.preventDefault();
    // const data = Object.fromEntries(new FormData(event.target).entries());
    deleteUser(id).then(function (response) {
      setgoViewShop(true);
    });
  };
  useEffect(() => {
    getShopById(id).then((data) => {
      setDevise(data.devise);
      // setAdmin(data.isAdmin);
      setshopSelected(data);
    });

    getExchangeRate().then((data) => {
      setExchangeRate(data);
    })

  }, []);

  return (
    <div>
      {show && (
        <AlertBar hidePop={hidePop} variant="success">
          Modification realisé
        </AlertBar>
      )}
      {goViewShop && <Redirect to={`/home/`} />}
      <h1> Magasins {shopSelected.nomSociete}</h1>
      {shopSelected !== false && (
        <Form onSubmit={handleSubmit}>
          <Form.Row>
            <Col>
              <Form.Group>
                <Form.Label>Nom societe :</Form.Label>
                <Input
                  type="text"
                  name="nomSociete"
                  defaultValue={shopSelected.nomSociete}
                  placeholder="Nom societe"
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                {/* <Form.Label>Devise :</Form.Label> */}
                {/* <Input  type="text" name="devise" defaultValue={shopSelected.devise} placeholder="Devise" /> */}
                <Form.Group>
                  <InputLabel id="demo-customized-select-label">
                    Devise
                  </InputLabel>
                  <Select
                    labelId="demo-customized-select-label"
                    id="demo-customized-select"
                    name="devise"
                    value={devise}
                    onChange={handleChange}
                  >
                    {/* <MenuItem value={"Euros"}>Euros</MenuItem>
                    <MenuItem value={"Dolars"}>Dolars</MenuItem>
                    <MenuItem value={"Roupi"}>Roupi</MenuItem>
                    <MenuItem value={"Livre Stairlings"}>
                      Livre Stairlings
                    </MenuItem> */}
                    {
              exchangeRate.map
              ((object) => (
                          <MenuItem key={object._id} value={
                                object.name
                                }>{
                                object.name
                        }</MenuItem>
              ))
    }
                  </Select>
                </Form.Group>
              </Form.Group>
            </Col>
          </Form.Row>
          <Container>
            <Row>
              <Col md={{ span: 4, offset: 7 }}>
                <Image src={shopSelected.kbis} rounded />
              </Col>
            </Row>
          </Container>
          <Button block type="submit">
            Modifier
          </Button>
          {/* <Button block  variant="danger" onClick={ () => { deleteMyUser() } }>
      Suprimer
      </Button> */}
        </Form>
      )}
    </div>
  );
};

export default DetailShop;
