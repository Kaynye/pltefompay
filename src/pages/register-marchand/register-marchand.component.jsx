import React, { useState,useEffect } from "react";
import { Button, Form } from "react-bootstrap";

import useAuth from "../../hooks/useAuth";
import { addShop } from "../../actions/addShop";
import AlertBar from "../../components/alert-bar/alert-bar.component";
import { getExchangeRate } from "../../actions/ExchangeRate";
import {getUsers} from "../../actions/getUsers";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import "./register-marchand.styles.scss";

export default function Login() {
  const [nomSociete, setnomSociete] = useState("");
  const [kbis, setKbis] = useState("");
  const [devise, setDevise] = useState("");
  const [contact, setContact] = useState("");
  const [exchangeRate, setExchangeRate] = useState([]);
  const { selectors, actions, selectId } = useAuth();
  const [showCreate, setShowCreate] = useState(false);
  // const [show, setShow] = useState(false);
  // console.log(selectId.getId());

  function handleSubmit(event) {
    event.preventDefault();
    const data = Object.fromEntries(new FormData(event.target).entries());

    addShop({ ...data, userId: sessionStorage.idU });
    // console.log(selectId.getId())
    // register(data);
    // actions({ ...data }).then((data)=>{

      setShowCreate(true);

    // });
  }

  function encodeImageFileAsURL(element) {
    console.log("je suis dedans !!!!!!!");
    // console.log(element)
    var file = element.files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
      setKbis(reader.result);
    };
    reader.readAsDataURL(file);
  }

  const hidePopCreate = () => {
    setShowCreate(false);
  }

  useEffect(() => {

    getExchangeRate().then((data) => {
      setExchangeRate(data);
    })
  }, []);
  
  return (
    <>
    {showCreate && (
        <AlertBar variant="success" hidePop={hidePopCreate}>
          La boutique est créé avec succés
        </AlertBar>
      )}
    <Form onSubmit={handleSubmit}>
      <Form.Group>
        <Form.Label>Nom de l'entreprise</Form.Label>
        <Form.Control
          type="text"
          value={nomSociete}
          onChange={(event) => {
            setnomSociete(event.target.value);
          }}
          name="nomSociete"
          placeholder="Entreprise"
        />
      </Form.Group>
      <Form.Group>
        <Form.File
          onChange={(event) => {
            encodeImageFileAsURL(event.target);
          }}
          id="exampleFormControlFile1"
          label="Example file input"
        />
      </Form.Group>
      <Form.Group controlId="exampleForm.ControlSelect1">
        <Form.Label>Devise</Form.Label>
        <Select
          name="devise"
          as="select"
          value={devise}
          onChange={(event) => {
            setDevise(event.target.value);
          }}
        >
          {/* <option>Euros</option>
          <option>Dolars</option>
          <option>Roupi</option>
          <option>Livre Stairlings</option>
          <option>Yen</option> */}
          {
              exchangeRate.map
              ((object) => (
                          <MenuItem key={object._id} value={
                                object.name
                                }>{
                                object.name
                        }</MenuItem>
              ))
    }
        </Select>
      </Form.Group>
      <Form.Group controlId="exampleForm.ControlTextarea1">
        <Form.Label>Contact</Form.Label>
        <Form.Control
          value={contact}
          name="contact"
          onChange={(event) => {
            setContact(event.target.value);
          }}
          as="textarea"
          rows="3"
        />
      </Form.Group>
      <Form.Control
        type="text"
        hidden
        value={kbis}
        name="kbis"
        placeholder="Entreprise"
      />

      <Button type="submit">Ajouter shop</Button>
    </Form>
    </>
  );
}
