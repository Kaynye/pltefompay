import React, {useState} from 'react';
import {
    Form,
    Button,
    FormGroup,
    FormControl,
    FormLabel,
  } from "react-bootstrap";
  import AlertBar from "../../components/alert-bar/alert-bar.component";

export default function CardPaiement() {

  const [showAnnuler, setShowAnnuler] = useState(false);
  const [showValide, setShowValide] = useState(false);
  const [show, setShow] = useState(false);


  const hidePop = () => {
    setShow(false);
  }

    return (
        <>

        {showValide && (
          <AlertBar variant="success" hidePop={hidePop}>
            Commande validé
          </AlertBar>
        )}

        <h1>Paiement</h1>
          <Form>
            <FormGroup>
              <FormControl
                controlId="card-number"
                placeholder="Numéro de carte"
              />
            </FormGroup>
            <FormGroup>
              <FormControl
                controlId="card-date"
                placeholder="mm/aa"
              />
            </FormGroup>
            <FormGroup>
              <FormControl
                controlId="card-crypto"
                placeholder="XXX"
              />
            </FormGroup>

            <Button block bsSize="medium" onClick={() => setShowValide(true)}>Valider</Button>
            <Button block bsSize="medium" onClick={() => console.log("Annuler")}>Annuler</Button>
          </Form>
        </>
    );
}
