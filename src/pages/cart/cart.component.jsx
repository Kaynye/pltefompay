import React, {useState} from 'react';
import { Button } from 'react-bootstrap';
import { Redirect } from "react-router-dom";
import { addTransaction } from '../../actions/addTransaction';
import AlertBar from "../../components/alert-bar/alert-bar.component";

const Cart = () => {

  const [showPaiement, setShowPaiement] = useState(false);
  const [cart, setCart] = useState({});
  const [message, setMessage] = useState("");

  const hidePop = () => {
    setShowPaiement(false);
  }
  
  const products = {
    pantalon: 25,
    chemise: 20,
    teeshirt: 12,
  };
   
  const createTransaction = (cart)=> {
    let price = 0;

    Object.keys(cart).map((product) => {
      price += products[product]*cart[product];
    });
    addTransaction(
      JSON.stringify(
        {prix:price, numclient:sessionStorage.idU, marchandId:"1"}
        )).then( (data) => {
        // setShow(true);
        return <Redirect to="/register" />;
    });
    // setShowPaiement(true);
  };

    return (
      <>
      {showPaiement && <Redirect to="/paiement" />}
        <ul>
          {Object.keys(products).map((product) => {
            return (
              <li>
                {product} ({products[product]}€){" "}
                <a onClick={()=> {
                    setCart({
                      ...cart,
                      [product]:
                      cart[product] && cart[product] > 0 ? cart[product] - 1: 0,
                    });
                  }}
                > - </a>
                {cart[product] || 0}
                <a onClick={() => {
                    setCart({
                      ...cart,
                      [product]: (cart[product] || 0) + 1,
                    });
                  }}
                  > + </a>
              </li>
            );
          })}
        </ul>
        <Button block bsSize="medium" onClick={() => createTransaction(cart)}>Payer</Button>
      </>
    );
}

export default Cart;