import React,{useState,useEffect} from 'react';
import {
    Redirect
  } from "react-router-dom";
import { BootstrapTable, TableHeaderColumn,CheckBoxForTable  } from 'react-bootstrap-table';

import 'react-bootstrap-table/css/react-bootstrap-table.css';

import {getUsers} from "../../actions/getUsers";

export default function HomeAdmin() {
  const [listeUser, setListeUser] = useState([]);
  const [userSelectedId, setUserSelected] = useState(null);
  const [goViewUser, setGoViewUser] = useState(false);
  const options = {
    onRowClick: function(row){
      setUserSelected(row.id);
      setGoViewUser(true);
    }
  };
  
  useEffect(
    () => {
      getUsers().then( (data)=> {
        setListeUser(data);
      } );
    },
    [],
  );
  return (
    <>
        <h1>
        Home HomeAdmin
    </h1>
    {goViewUser  && <Redirect to={`/view-user/`+userSelectedId } /> }
    <BootstrapTable
  data={listeUser}
  striped
  hover
  condensed
  search
  // cellEdit={ cellEditProp }
  // selectRow={ selectRow }
  exportCSV
  options={ options }
>
  <TableHeaderColumn dataField="id" isKey  dataSort>id</TableHeaderColumn>
  <TableHeaderColumn dataField="firstname" editable={false}  dataSort>Nom</TableHeaderColumn>
  <TableHeaderColumn dataField="lastname" editable={false}  dataSort>Prenom</TableHeaderColumn>
  <TableHeaderColumn dataField="username" editable={false}  dataSort>Email</TableHeaderColumn>
  <TableHeaderColumn dataField="updatedAt" editable={false}   dataFormat={format}>Membre depuis</TableHeaderColumn>
  <TableHeaderColumn dataField='actif' dataFormat={ activeFormatter } editable={ { type: 'checkbox', options: { values: 'Y:N' } } }>Actif</TableHeaderColumn>
  <TableHeaderColumn dataField='isAdmin' dataFormat={ activeFormatter } editable={ { type: 'checkbox', options: { values: 'Y:N' } } }>Admin</TableHeaderColumn>
</BootstrapTable>
    </>
   
  );

}

const selectRow = {
  mode: 'checkbox', //radio or checkbox,
  clickToSelect: true

};

const cellEditProp = {
  mode: 'click',
  blurToSave: true
};


function format(cell, row){
  return new Date(cell).toLocaleString();
}



class ActiveFormatter extends React.Component {
  render() {
    return (
      <input type='checkbox' checked={ this.props.active }/>
    );
  }
}

function activeFormatter(cell, row) {
  return (
    <ActiveFormatter active={ cell } />
  );
}