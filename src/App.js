import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Login from "./pages/login/login.component";
import Register from "./pages/register/register.component";
import Home from "./pages/home/home.component";
import HomeAdmin from "./pages/home-admin/home-admin.component";
import Transactions from "./pages/transactions/transactions.component";
import RegisterMarchand from "./pages/register-marchand/register-marchand.component";
import DetailUser from "./pages/detail-user/detail-user.component";
import DetailShop from "./pages/detail-shop/detail-shop.component";
import Header from "./components/header/header.component";
import Cart from "./pages/cart/cart.component";
import CardPaiement from "./pages/cart/card-paiement.component";
// import useAuth from "./hooks/useAuth";

const App = () => {
  // const { selectors, actions } = useAuth();
  return (
    <Router>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route
          path="/home"
          render={(props) => (
            <div>
              <Header />
              <Home />
            </div>
          )}
        />
        <Route
          path="/home-admin"
          render={(props) => (
            <div>
              <Header />
              <HomeAdmin />
            </div>
          )}
        />
        <Route
          path="/transactions"
          render={(props) => (
            <div>
              <Header />
              <Transactions />
            </div>
          )}
        />
        <Route
          path="/new-shop"
          render={(props) => (
            <div>
              <Header />
              <RegisterMarchand />
            </div>
          )}
        />
        <Route
          path="/view-user/:id"
          render={(props) => (
            <div>
              <Header />
              <DetailUser />
            </div>
          )}
        />
        <Route
          path="/view-shop/:id"
          render={(props) => (
            <div>
              <Header />
              <DetailShop />
            </div>
          )}
        />

        <Route
          path="/cart"
          render={(props) => (
            <div>
              <Header />
              <Cart />
            </div>
          )}/>

        <Route
          path="/paiement"
          render={(props) => (
            <div>
              <Header />
              <CardPaiement />
            </div>
          )}/>
        <Redirect strict from="/" to="/login" />
      </Switch>
    </Router>
  );
};

export default App;
