import { useReducer } from "react";
// import BoardContext from "../contexts/boardContext";
import { login } from "../contexts/actions/auth";

// const BoardContext = createContext(null);

// const userContext=createContext({
//     auth: false,
//     token:""
// });

const reducer = (state, action) => {
    // console.log(action)
    switch (action.type) {
      case "CONNECT":
        return {
          ...state,
          token: action.payload.token,
          idU: action.payload.idU,
          auth: action.payload.auth,
          isAdmin: action.payload.isAdmin,
        };
      default:
        return state;
    }
  };

const useAuth = () => {
    const [state, dispatch] = useReducer(reducer, {
        auth: sessionStorage.auth!==undefined   || false,
        idU: sessionStorage.idU!==undefined   || false,
        isAdmin: sessionStorage.isAdmin!==undefined   || false,
        token:sessionStorage.tokenReact || ""
    });

    const  actions = async (dataConnex) => {
        let a = await login(dataConnex).then((data) =>{
            if (data===false){
              return false;
            }
            else{
              let isAdmin=data.isAdmin || false;
              dispatch({
                type: "CONNECT",
                // eslint-disable-next-line eqeqeq
                payload: {...data, auth: data.token!=undefined, idU:data.idU,isAdmin: isAdmin },
              })
              sessionStorage.setItem("tokenReact",data.token)
              sessionStorage.setItem("idU",data.idU)
              sessionStorage.setItem("isAdmin",isAdmin)
            }
          }
        )
        // console.log(a);
    };

//   const actions = {
//     login: (data) => {
//       login(data).then((data) =>
//         dispatch({
//           type: "LOGIN",
//           payload: {
//             token: data.token,
//           },
//           audiance: "auth",
//         })
//       );
//     },
//   };

  const selectors = {
    isLogged: () => state.auth,
    isAdmin: () => state.isAdmin,
  };
  
  const selectId = {
    getId: () => state.idU,
  };
  return { selectors, actions, selectId };
};

export default useAuth;