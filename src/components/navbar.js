import React, { useState } from "react";
// import './App.css';
import {  Link } from 'react-router-dom';
// import { useState } from 'react';
import { Navbar,Nav } from "react-bootstrap"
import {
    Redirect
  } from "react-router-dom";
  import useAuth from "../hooks/useAuth";

// let isAdmin=sessionStorage.isAdmin;
function Navebar() {
    const [connected, disconected] = useState(true);
    function logOut() {
        sessionStorage.removeItem("tokenReact");
        sessionStorage.removeItem("idU");
        sessionStorage.removeItem("isAdmin");
        sessionStorage.removeItem("isMarchand");
        disconected(false)
    }
  return (
    <>
    {!connected  && <Redirect to="/admin/login" /> }

  <Navbar bg="dark" variant="dark">
    <Navbar.Brand href="#home">
      <img
        alt=""
        src="/logo.svg"
        width="30"
        height="30"
        className="d-inline-block align-top"
      />{' '}
      Payemement VS
    </Navbar.Brand>
    <Nav className="mr-auto">
      {
        sessionStorage.isAdmin=='true' ?  <>
          <Nav.Link as={Link}  to="/homeAdmin">Home</Nav.Link>
          <Nav.Link as={Link}  to="/transactions">valider</Nav.Link>
          {/* <Nav.Link as={Link}  to="/newShop">Nouveau Shop</Nav.Link> */}
          <Nav.Link  onClick={logOut} >Se deconnecter</Nav.Link>
        </> :
          sessionStorage.isMarchand=='true' ? <>
        <Nav.Link as={Link}  to="/home">Home</Nav.Link>
          </> :
        <>
        <Nav.Link as={Link}  to="/home">Home</Nav.Link>
        <Nav.Link as={Link}  to="/transactions">transactions</Nav.Link>
        <Nav.Link as={Link}  to="/newShop">Nouveau Shop</Nav.Link>
        <Nav.Link as={Link}  to="/cart">Panier</Nav.Link>
        <Nav.Link  onClick={logOut} >Se deconnecter</Nav.Link>
      </>
      }
    </Nav>
  </Navbar>
</>
  );
}

export default Navebar;
