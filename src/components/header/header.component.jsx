import React, { useState } from "react";
import { Link } from "react-router-dom";
// import { useState } from 'react';
import { Navbar, Nav } from "react-bootstrap";
import { Redirect } from "react-router-dom";
// import useAuth from "../../hooks/useAuth";
import './header.styles.scss';
// let isAdmin=sessionStorage.isAdmin;
const Header = () => {
  const [connected, disconected] = useState(true);
  
  function logOut() {
    sessionStorage.removeItem("tokenReact");
    sessionStorage.removeItem("idU");
    sessionStorage.removeItem("isAdmin");
    disconected(false);
  }
  return (
    <>
      {!connected && <Redirect to="/admin/login" />}
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">
          <img
            alt=""
            src="/logo.svg"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{" "}
          Payemement VS
        </Navbar.Brand>
        <Nav className="mr-auto">
          {sessionStorage.isAdmin === "true" ? (
            <>
              <Nav.Link as={Link} to="/home-admin">
                Home
              </Nav.Link>
              <Nav.Link as={Link} to="/transactions">
                Valider
              </Nav.Link>
              {/* <Nav.Link as={Link}  to="/newShop">Nouveau Shop</Nav.Link> */}
              <Nav.Link onClick={logOut}>Se deconnecter</Nav.Link>
            </>
          ) : (
            <>
              <Nav.Link as={Link} to="/home">
                Home
              </Nav.Link>
              <Nav.Link as={Link} to="/transactions">
                Transactions
              </Nav.Link>
              <Nav.Link as={Link} to="/new-shop">
                Nouveau Shop
              </Nav.Link>
              <Nav.Link onClick={logOut}>Se deconnecter</Nav.Link>
            </>
            ) }
        </Nav>
      </Navbar>
    </>
  );
};

export default Header;
