import React, { useState } from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";

// function Alert(props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  alert: {
    fontSize: "14pt",
    fontWeight: "bold",
  },
}));

const AlertBar = ({ children, variant,hidePop }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
    hidePop();
  };

  

  return (
    <div>
      <Snackbar open={open} autoHideDuration={10000} onClose={handleClose}>
        <MuiAlert
          className={classes.alert}
          onClose={handleClose}
          severity={variant}
        >
          {children}
        </MuiAlert>
      </Snackbar>
    </div>
  );
};

export default AlertBar;
