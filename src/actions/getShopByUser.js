export const getShopByUser = async (id) => {
  let token="Bearer "+sessionStorage.tokenReact;
  return fetch(`http://localhost:3000/users/shop/${id}`, {
    method: "GET",
    headers: { 
      "Content-type": "application/json",
     "Accept" : "application/json",
     "Authorization" : token,
     },
  }).then((res) => res.json());
}
  