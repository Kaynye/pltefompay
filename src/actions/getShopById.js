export const getShopById = async (id) => {
  let token="Bearer "+sessionStorage.tokenReact;
  return fetch(`http://localhost:3000/marchand/${id}`, {
    method: "GET",
    headers: { 
      "Content-type": "application/json",
     "Accept" : "application/json",
     "Authorization" : token,
     },
  }).then((res) => res.json());
}
  