export const getUserById = async (id) => {
  let token="Bearer "+sessionStorage.tokenReact;
  return fetch(`http://localhost:3000/users/${id}`, {
    method: "GET",
    headers: { 
      "Content-type": "application/json",
     "Accept" : "application/json",
     "Authorization" : token,
     },
  }).then((res) => res.json());
}
  