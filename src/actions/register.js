export const register = async (param) => {
  let a = await fetch(`http://localhost:3000/register`, {
    method: "POST",
    body: JSON.stringify(param),
    headers: { "Content-type": "application/json" },
  }).then((res) => {
    if (res.status===401){
      return false;
    }
     return res.json()
     }
  
  );
  return a;
}
  