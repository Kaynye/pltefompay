export const addTransaction = async( param ) => {
    let autoriz="Bearer "+sessionStorage.tokenReact
    let a = await fetch(`http://localhost:3000/transaction`, {
        method: "POST",
        headers: { "Content-type": "application/json","Authorization": autoriz},
        body: param,
    }).then((res) => {
        if (res.status===401){
            // console.log(res);
            return false;
        }
        return res.json()
        }

    );
    return a;
}