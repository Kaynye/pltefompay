export const addShop = async (param) => {
  let autoriz="Bearer "+sessionStorage.tokenReact
  let a = await fetch(`http://localhost:3000/marchand`, {
    method: "POST",
    body: JSON.stringify(param),
    headers: { "Content-type": "application/json","Authorization": autoriz},
  }).then((res) => {
    if (res.status===401){
      return false;
    }
     return res.json()
     }
  
  );
  return a;
}
  