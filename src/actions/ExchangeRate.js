export const getExchangeRate = async () => {
  let token="Bearer "+sessionStorage.tokenReact;
  return fetch(`http://localhost:3000/exchange`, {
    method: "GET",
    headers: { 
      "Content-type": "application/json",
     "Accept" : "application/json",
     "Authorization" : token,
     },
  }).then((res) => res.json());
}
  