export const login = (param) =>
  fetch(`http://localhost:3000/login`, {
    method: "POST",
    body: JSON.stringify(param),
    headers: { "Content-type": "application/json" },
  }).then((res) => {
    if (res.status===401){
      return false;
    }
     return res.json()
     }
  );