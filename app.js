const express = require("express");
const UserRouter = require("./server/routes/users");
const MarchandRouter = require("./server/routes/marchand");
const TransactionRouter = require("./server/routes/transaction");
const ExchangeRouter = require("./server/routes/Exchange");
const SecurityRouter = require("./server/routes/security");
const verifyJwt = require("./server/middlewares/verifyJwt");
const cors = require("cors");

const app = express();

// middlewares
app.use(express.json());
app.use(cors());

// routers
app.get("/hello", (req, res) => {
  res.json({ msg: "Hello" });
});
app.use(SecurityRouter);
app.use(verifyJwt);
app.use("/users", UserRouter);
app.use("/marchand", MarchandRouter);
app.use("/transaction", TransactionRouter);
app.use("/exchange", ExchangeRouter);
// app.use("/marchand", UserRouter);
// app.use("/movies", MovieRouter);

app.listen(3000, () => console.log("listening..."));