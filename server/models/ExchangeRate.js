const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
    name: String,
    rate: Number,
    url: String,
    source: String


},{
    collection : 'ExchangeRate'
});

const model = mongoose.model('ExchangeRate', Schema);

module.exports = model;