'use strict';
const {
  Model
} = require('sequelize');


module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Transaction.belongsTo(models.Marchand, {
        foreignKey: 'marchandId',
        onDelete: 'CASCADE'
      })
    }
  };
  Transaction.init({
    prix: DataTypes.INTEGER,
    numclient: DataTypes.STRING,
    marchandId: DataTypes.INTEGER,
    validated: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Transaction',
  });
  return Transaction;
};