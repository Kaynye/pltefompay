'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require("bcrypt");
const denormalize = require("./denormalisations/user");


module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Marchand, {
        foreignKey: 'userId',
      })
    }
  };
  User.init({
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isAdmin: DataTypes.BOOLEAN,
    actif: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'User',
  });

  User.addHook("beforeCreate", async (user) => {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
  });
  


  User.addHook("afterCreate", (user) => {
  denormalize(user, "create");
});
User.addHook("afterUpdate", (user) => {
  denormalize(user, "update");
});
User.addHook("afterDestroy", (user) => {
  denormalize(user, "delete");
});





  
  return User;
};