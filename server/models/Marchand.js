'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Marchand extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Marchand.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      })
      Marchand.hasMany(models.Transaction, {
        foreignKey: 'id',
      })
    }
  };
  Marchand.init({
    nomSociete: DataTypes.STRING,
    kbis: DataTypes.STRING(10485760),
    contact: DataTypes.STRING,
    urlconfirmation: DataTypes.STRING,
    urlannulation: DataTypes.STRING,
    devise: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Marchand',
  });
  return Marchand;
};