const fs = require('fs');
const Scrapper = require('../server/lib/Scrapper');
const connection = require('../server/lib/db.js');
const ExchangeRate = require('../server/models/ExchangeRate');
const url = 'https://www.x-rates.com/table/?from=EUR&amount=1';

const processData2 = ($) => {
    const data = [];

    $('.ratesTable tr').each((index, tr)=> {
        
        let name=$( $(tr).children()[0]).text()
        let rate=$($( $(tr).children()[1]).children()[0]).text()
        let urlData=$($( $(tr).children()[1]).children()[0]).attr('href')
        data.push({
            "name": name,
            "rate": rate,
            "url": urlData,
            "source": url
        })
    });
    return data;
};

const saveResult2 = data => {
    Promise.all(data.map(datum => {
        const exchangerate = new ExchangeRate(datum);
        return exchangerate.save();
    })).then(() => console.log('data saved'));
};

const req2 = Scrapper('https://www.x-rates.com/table/?from=EUR&amount=1', {}, processData2, saveResult2);
req2.end();
