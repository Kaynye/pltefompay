  
const  {User,Marchand}  = require("../models/");
const bcrypt = require("bcrypt");
const verifyJwt = require("../middlewares/verifyJwt");

const Router = require("express").Router;
const createToken = require("../lib/jwt").createToken;
const sendRegister = require("../lib/mailer");
const router = Router();

router.post("/login", (req, res) => {
  const { username, password } = req.body;
  User.findOne({
    where: { username, actif:true },
  }).then((user) => {
    if (!user) {
      res.status(401).json({
        username: ["Invalid credentials"],
        password: ["Invalid credentials"],
      });
    } else {
      // console.log(user);
      bcrypt.compare(password, user.password).then((valid) => {
        if (valid) {
          createToken({ username })
            .then((token) => res.json({ token,idU:user.id,isAdmin: user.isAdmin, isMarchand:user.isMarchand }))
            .catch(() => res.sendStatus(500));
        } else {
          res.status(401).json({
            username: ["Invalid credentials"],
            password: ["Invalid credentials"],
          });
        }
      });
    }
  });
});

// POST
router.post("/register", (req, res) => {
  User.create(req.body)
    .then((user) => {
      
      // createToken({username:req.body.username}).then((token) => {
      //   res.status(201).json({ token, idU:user.id }) 
      // })
      // .catch((e) => console.log(e));
      sendRegister(user.username);
      res.status(201).json({ created: true }) 
    })
    .catch((err) => {
      console.log(err);
      if (err.name === "SequelizeValidationError") {
        res.status(400).json(prettifyErrors(Object.values(err.errors)));
      } else {
        // console.log(err);
        res.sendStatus(500);
      }
    });
});

// router.post("/marchand", (req, res) => {
//   // console.log(req);
//   Marchand.create(req.body)
//     .then((user) => res.status(201).json(user))
//     .catch((err) => {
//       if (err.name === "SequelizeValidationError") {
//         res.status(400).json(prettifyErrors(Object.values(err.errors)));
//       } else {
//         console.log(err);
//         res.sendStatus(500);
//       }
//     });
// });

// router.get("/users", (req, res) => {
//   verifyJwt(req,res,function(){
//     User.findAll({})
//     .then((user) => res.status(201).json(user))
//     .catch((err) => {
//       if (err.name === "SequelizeValidationError") {
//         res.status(400).json(prettifyErrors(Object.values(err.errors)));
//       } else {
//         console.log(err);
//         res.sendStatus(500);
//       }
//     });
//   })

  
// });

const prettifyErrors = (errors) => {
    return errors.reduce((acc, item) => {
      acc[item.path] = [...(acc[item.path] || []), item.message];
  
      return acc;
    }, {});
  };

module.exports = router;