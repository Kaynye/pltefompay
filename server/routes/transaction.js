const Router = require("express").Router;
const { User,Marchand,Transaction } = require("../models");
const { Op } = require("sequelize");
// const Article = require("../models/sequelize/Article");
const router = Router();

/** Collection Routes  **/
// CGET
// or[username]=kmarques&or[password]=test&lastname=null
// and[0][or][A]=kmarques&and[0][or][B]=test&and[1][or][C]=kmarques&and[1][or][D]=test
router.get("/", (req, res) => {
  Transaction.findAll()
    .then((result) => res.json(result))
    .catch((error) => console.log(error));
});

router.get("/:userId", (req, res) => {
  Transaction.findAll({
    include:[{
      model: Marchand,
      where: {
        userId: req.params.userId
      }
    }]
  })
    .then((result) => res.json(result))
    .catch((error) => console.log(error));
});

/** Item Routes  **/
router.get("/:id", (req, res) => {
  Transaction.findByPk(req.params.id)
    .then((data) => (data ? res.json(data) : res.sendStatus(404)))
    .catch(() => res.sendStatus(500));
});

router.put("/:id", (req, res) => {

  Transaction.update(req.body, {
    returning: true,
    where: { id: req.params.id },
  })
    .then(([nbUpdate, data]) =>
      nbUpdate === 1 ? res.json(data) : res.sendStatus(404)
    )
    .catch((err) => {
      if (err.name === "SequelizeValidationError") {
        res.status(400).json(prettifyErrors(Object.values(err.errors)));
      } else {
        res.sendStatus(500);
      }
    });
});

router.post("/", (req, res) => {
  Transaction.create(req.body)
    .then((user) => res.status(201).json(user))
    .catch((err) => {
      if (err.name === "SequelizeValidationError") {
        res.status(400).json(prettifyErrors(Object.values(err.errors)));
      } else {
        console.log(err);
        res.sendStatus(500);
      }
    });
});

router.delete("/:id", (req, res) => {
  Transaction.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((nbDelete) => (nbDelete ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => res.sendStatus(500));
});

const prettifyErrors = (errors) => {
  return errors.reduce((acc, item) => {
    acc[item.path] = [...(acc[item.path] || []), item.message];

    return acc;
  }, {});
};

module.exports = router;