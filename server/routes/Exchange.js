const Router = require("express").Router;
const ExchangeRate = require('../models/ExchangeRate');
const { Op } = require("sequelize");
// const Article = require("../models/sequelize/Article");
const router = Router();

/** Collection Routes  **/
// CGET
// or[username]=kmarques&or[password]=test&lastname=null
// and[0][or][A]=kmarques&and[0][or][B]=test&and[1][or][C]=kmarques&and[1][or][D]=test
router.get("/", (req, res) => {
  ExchangeRate.find()
  .then((data) => res.json(data))
  .catch((err) => res.sendStatus(500));

});

module.exports = router;