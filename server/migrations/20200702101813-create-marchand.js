'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Marchands', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nomSociete: {
        type: Sequelize.STRING
      },
      kbis: {
        type: Sequelize.STRING(10485760)
      },
      contact: {
        type: Sequelize.STRING
      },
      urlconfirmation: {
        type: Sequelize.STRING
      },
      urlannulation: {
        type: Sequelize.STRING
      },
      devise: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'Users',
          key: 'id',
          as: 'userId',
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Marchands');
  }
};