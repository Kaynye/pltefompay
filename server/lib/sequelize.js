const Sequelize = require("sequelize");

const connection = new Sequelize("postgres://root:password@:5434/react",{
  // logging: console.log
});
connection
  .authenticate()
  .then(() => console.log("connected to PG"))
  .catch((err) => console.error(""));

module.exports = connection;