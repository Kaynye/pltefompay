const jwt = require("jsonwebtoken");

const createToken = (payload) => {
  console.log("payload")
  console.log(payload)
  return new Promise((resolve, reject) => {
    jwt.sign(
      payload,
      "MyBestSecret",
      {
        algorithm: "HS256",
        expiresIn: 3600,
      },
      (err, token) => {
        if (err) reject();
        resolve(token);
      }
    );
  });
};

const verifyToken = (token) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, "MyBestSecret", (err, payload) => {
      if (err) reject(err);
      resolve(payload);
    });
  });
};

module.exports = {
  createToken,
  verifyToken,
};